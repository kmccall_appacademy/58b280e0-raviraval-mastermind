# require 'byebug'
class Code
  attr_reader :pegs

  PEGS = {
    "B" => :blue,
    "G" => :green,
    "O" => :orange,
    "P" => :purple,
    "R" => :red,
    "Y" => :yellow
  }

  def initialize(peg_array)
    @pegs = peg_array
  end

  def self.random
    rand_array = []
    4.times { rand_array << PEGS.keys.sample}
    Code.new(rand_array)
  end

  def self.parse(input)
    input.upcase.chars.each do |ch|
      unless PEGS.keys.include?(ch)
        raise "Invalid Colors"
      end
    end
    Code.new(input.upcase.chars)
  end

  def exact_matches(code)
    matches_counter = 0
    self.pegs.each_with_index do |peg, idx|
      matches_counter += 1 if peg == code.pegs[idx]
    end
    matches_counter
  end

  def near_matches(code)
    dupped_secret = self.pegs.dup.sort
    dupped_given = code.pegs.dup.sort
    matches_counter = 0

    dupped_given.each_with_index do |peg, idx|
      if dupped_secret.include?(peg)
        matches_counter += 1
        dupped_secret.shift
      end
    end

    matches_counter - exact_matches(code)
  end

  def ==(code)
    if code.class == Code
      return self.pegs == code.pegs
    end
    false
  end

  def [](idx)
    pegs[idx]
  end

end

class Game
  attr_reader :secret_code, :guesses

  def initialize(secret_code = Code::random)
    @secret_code = secret_code
    @guesses = 0
  end

  def get_guess
    puts
    puts "Guess the code! Ex: \"BBBB\" or \"bbbb\""
    puts "The colors available are B, G, O, P, R, Y."
    begin
      Code::parse($stdin.gets.chomp)
    rescue
      puts "Wrong input"
      retry
    end
  end

  def display_matches(code)
    puts "#{@secret_code.exact_matches(code)} exact matches."
    puts "#{@secret_code.near_matches(code)} near matches."
  end

  def play
    puts "Welcome to Mastermind!"
    puts
    while true
      if @guesses < 9
        puts "You have #{10 - guesses} guesses left."
      else
        puts "You're on your last guess! Good luck.."
      end
      puts
      user_code = get_guess
      puts
      display_matches(user_code)
      @guesses += 1
      if @guesses == 10
        lose
      end
      if user_code == @secret_code
        won
      end
    end
  end

  def won
    puts
    puts "Congratulations! You guessed the right code."
    exit
  end

  def lose
    puts
    puts "Too bad. You ran out of guessses."
    puts "The correct code was #{secret_code.pegs.join("")}."
    exit
  end
end

if $PROGRAM_NAME == __FILE__
  Game.new().play
end
